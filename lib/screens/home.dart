import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:tinder_card/main.dart';
import 'package:tinder_card/models/custom_text.dart';


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {

  CardSwipeOrientation rotate = CardSwipeOrientation.LEFT;
  int i = 0;

List<String> welcomeImages = [
  "assets/capture.png",
  "assets/capture1.png",
  "assets/capture2.png",
  "assets/capture3.png",
  "assets/capture4.png",
  "assets/capture5.png",
  "assets/capture6.png",
  "assets/femme.png",
  "assets/map.jpg",
];

List<String> profile = [
  "assets/images1.jpeg",
  "assets/images2.jpeg",
  "assets/images3.jpeg",
  "assets/images4.jpeg",
  "assets/images5.jpeg",
  "assets/images6.jpeg",
  "assets/images7.jpg",
  "assets/images8.jpg"
];

List<String> listNom = [
  "Gregory Smith",
  "Angelina slow",
  "Estrella moone",
  "Maguerita love",
  "Augustus Evans",
  "Hummels jid",
  "Guf Edti",
  "Johson Fit",
  "Dane Keat"
];


  @override
  Widget build(BuildContext context) {

    CardController controller;

    return Scaffold(
      appBar: AppBar(
        title: Text("Moi"),
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.transparent,
      body: RefreshIndicator(
          onRefresh: () {
            Navigator.pushReplacement(
              context, PageRouteBuilder(pageBuilder: (a, b, c) =>
                MyApp(),
              transitionDuration: Duration(seconds: 0),),);
            return Future.value(false);
          },
      child: SingleChildScrollView(
        child: Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  
                ),
                Center(
                child: Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 150.0,
                  ),
                  height: MediaQuery.of(context).size.height * 0.7,
                  child: TinderSwapCard(
                    swipeDown: false,
                    swipeUp: false,
                    orientation: AmassOrientation.TOP,
                    totalNum: welcomeImages.length,
                    stackNum: 3,
                    swipeEdge: 10.0,
                    maxHeight: MediaQuery.of(context).size.width * 0.9,
                    maxWidth: MediaQuery.of(context).size.width * 0.9,
                    minHeight: MediaQuery.of(context).size.width * 0.8,
                    minWidth: MediaQuery.of(context).size.width * 0.8,

                    cardBuilder: (context, index) => Card(
                      elevation: 10.0,
                      child: Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(32)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: MediaQuery.of(context).size.width * 0.2,
                              color: Colors.grey[300],
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  padding(),
                                  ClipOval(
                                    child: Image(
                                      image: AssetImage("${welcomeImages[index]}"),
                                      height: 50.0,
                                      width: 50.0,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  padding(),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                    Text("${listNom[index]}", style: TextStyle(color: Colors.black),),
                                    Row(
                                      children: [
                                        Icon(Icons.star, color: Colors.yellow, size: 15.5),
                                        Text("4.9", style: TextStyle(color: Colors.grey[500])),
                                      ],
                                    ),
                                ],
                              ),
                              ClipOval(
                                child: Container(
                                  height: 40.0,
                                  width: 40.0,
                                  color: Colors.blue,
                                  child: Icon(
                                    AntDesign.message1,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                                ClipOval(
                                  child: Container(
                                    height: 40.0,
                                    width: 40.0,
                                    color: Colors.black,
                                    child: Icon(
                                      Icons.phone,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.width * 0.2,
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                padding(),
                                ClipOval(
                                child: Image(
                                  image: AssetImage("${profile[0]}"),
                                  height: 25.5,
                                  width: 25.5,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(right: 2.0)),
                              ClipOval(
                                child: Image(
                                  image: AssetImage("${profile[1]}"),
                                  height: 25.5,
                                  width: 25.5,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(right: 2.0)),
                              ClipOval(
                                child: Image(
                                  image: AssetImage("${profile[2]}"),
                                  height: 25.5,
                                  width: 25.5,
                                  fit: BoxFit.cover,
                                ),
                              ),
                                Padding(padding: EdgeInsets.only(right: 15.0)),
                              Text("25 Recommended"),
                              SizedBox(width:20.0),
                              ],
                            ),
                          ),
                          Container(
                            height: 1.0,
                            color: Colors.grey,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                            height: MediaQuery.of(context).size.width * 0.2,
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                //padding(),
                                Container(
                                  height: 30.0,
                                    width: 50.0,
                                    child: Image.asset("assets/voiture.png")
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("DISTANCE",
                                      style: TextStyle(color: Colors.grey[300], fontStyle: FontStyle.normal),
                                    ),
                                    Text("0.2 km", style: TextStyle(color: Colors.black, fontStyle: FontStyle.normal, fontSize: 17.0),),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("TIME",
                                      style: TextStyle(color: Colors.grey[300], fontStyle: FontStyle.normal),
                                    ),
                                    Text("2 min", style: TextStyle(color: Colors.black, fontStyle: FontStyle.normal, fontSize: 17.0),),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("PRICE",
                                      style: TextStyle(color: Colors.grey[300], fontStyle: FontStyle.normal),
                                    ),
                                    Text("\$25.00", style: TextStyle(color: Colors.black, fontStyle: FontStyle.normal, fontSize: 17.0),),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(right: 10.0, left: 10.0),
                            height: MediaQuery.of(context).size.width * 0.2,
                            color: Colors.white,
                            child: Center(
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                color: Colors.blueGrey[900],
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text("Confirm", textAlign: TextAlign.center, style: TextStyle(color: Colors.white),)
                                  ],
                                ),
                                onPressed: null,
                              ),
                            ),
                          ),
                        ],

                      ),
                    ),
                  ),
                  cardController: controller = CardController(),
                  swipeUpdateCallback: (DragUpdateDetails details, Alignment align) {
                    /// Get swiping card's alignment
                    if (align.x < 0) {

                      //Card is LEFT swiping
                    } else if (align.x > 0) {
                      //Card is RIGHT swiping
                    }
                  },
                  swipeCompleteCallback:
                      (CardSwipeOrientation orientation, int index) {
                    /// Get orientation & index of swiped card!
                    setState(() {
                      rotate = orientation;
                      i = index;

                      if(index  < 0 ) {
                        return MyHomePage();
                      }
                    });
                  },
                ),
                ),
              ),
              ] 
        ),
      ),
      ),
    );
  }

  Padding padding() {
    return Padding(
      padding: EdgeInsets.only(right: 5.0),
    );
  }

  Future<Null> refreshList() async {
    await Future.delayed(Duration(seconds: 2));
  }
}

